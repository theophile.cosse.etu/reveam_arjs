"use strict"

class Content {
  constructor(name) {
    this.name=name;
    this.node = new THREE.Object3D();
  }

  setPosition(x,y,z) {
    this.node.position.set(x,y,z);
  }

  getNode() {
    return this.node;
  }

  setDimensions(x,y,z) {
    this.node.scale.set(x,y,z);
  }

  press() {

  }

  move() {

  }

  release() {

  }

}


class Shape extends Content {
  constructor(name, shape) {
    super(name);
    this.geom = new THREE.BoxBufferGeometry(1, 1, 1);
    this.mat = new THREE.MeshBasicMaterial({color: 0xffffff, 
					    side:THREE.DoubleSide});
    this.mesh = new THREE.Mesh(this.geom, this.mat);
    this.setShape(shape);
    this.node.add(this.mesh);
    this.mesh.userData=this;
  }

  setShape(shape) {
    switch(shape) {
      case "box" : {
	this.mesh.geometry = new THREE.BoxBufferGeometry(1, 1, 1);
      }
      case "sphere" : {
	this.mesh.geometry = new THREE.SphereBufferGeometry(1, 20, 20);
      }
    }
  }

}


class Text extends Content {
  constructor(name, text) {
    super(name);
    this.text=text;
    this.depth=10;
    this.nbElements=10;
    let loader = new THREE.FontLoader();
    loader.load( '../data/fonts/font.json', function ( font ) {
	let geometry = new THREE.TextGeometry(this.text, {
					    font: font,
					    size: 25,
					    height: this.depth
					    } );
	let mat = new THREE.MeshBasicMaterial({color: 0xffffff, 
						side:THREE.DoubleSide});
	for(let i=0; i<this.nbElements; ++i) {
	  let text = new THREE.Mesh(geometry, mat);        
          text.userData=this;
	  text.position.set(0, 0, i*this.depth);
	  this.node.add(text);
	}
    }.bind(this));
  }

  setText(text) {
  }
}

class Layers extends Content {
  constructor(name, path, number) {

    super(name);
    let geom = new THREE.BoxGeometry(1,1,1/number);
    let loader = new THREE.TextureLoader();
    for(let l=0; l<number; l++) {
      let mat = new THREE.MeshBasicMaterial({color: 0xffffff, 
					  side:THREE.BackSide,
					  map:loader.load(path+l+".jpg")});
      console.log("loading ",path+number+".jpg");
      let mesh = new THREE.Mesh(geom, mat);
      mesh.userData=this;
      mesh.position.set(0, 0, 1/number*l);
      this.node.add(mesh);
    }
  }
}

class Model extends Content {
  constructor(name, model) {
    super(name);
      let loader = new THREE.OBJLoader()
      let loaded = loader.load(model, function(mod) {
	mod.children[0].material.side=THREE.DoubleSide;
        mod.userData=this;
	this.node.add(mod);
      }.bind(this));
  }
}

class Video extends Content {
  constructor(name, file) {
    super(name);
    this.video = document.createElement("VIDEO");
    this.video.id = "video_"+name;
    this.video.src = file;
    this.video.muted = true;
    document.body.appendChild(this.video);

    let texture  = new THREE.VideoTexture(this.video);
    texture.minFilter = THREE.LinearFilter;
    texture.magFilter = THREE.LinearFilter;
    texture.format = THREE.RGBFormat;

    let geom = new THREE.BoxGeometry(1,1,1);
    let mat = new THREE.MeshBasicMaterial({color: 0xffffff, 
                                            side:THREE.BackSide,
                                            map:texture});
    let mesh = new THREE.Mesh(geom, mat);
    mesh.userData=this;
    this.node.add(mesh);
    this.video.loop=true;
  }

  press() {
    if(this.video.paused) {
      this.video.play();
    }
    else {
      this.video.pause();
    }
  }
}

