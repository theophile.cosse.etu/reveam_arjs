import { DragControls } from './lib/DragControls.js';

let renderer = new THREE.WebGLRenderer({
	antialias: true,
	alpha: true
});

let grey = new THREE.Color( 0x858585 );
let white = new THREE.Color( 0xffffff );
renderer.domElement.style.position = 'absolute';
renderer.domElement.style.top = '0px';
renderer.domElement.style.left = '0px';
renderer.setClearColor(white, 0);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild( renderer.domElement );

// init scene and camera
let scene = new THREE.Scene();
let objList = [];
let sizeW=640;
let sizeH=480;

//scene.background = new THREE.Color( 0x858585 );
// Camera Perspective
let camera = new THREE.PerspectiveCamera(45, 640/480, 0.001, 500);
scene.add(camera);


// Camera Ortho
let cameraSlice = new THREE.OrthographicCamera(-sizeW/300, sizeW/300, 
	sizeH/300, -sizeH/300,
	0.0001, 1);
scene.add(cameraSlice);
let renderCamera = camera;

/// get video element
let video = null
window.addEventListener("arjs-video-loaded", videoloaded);

function videoloaded(param) {
	video = param.detail.component;
}


//  artoolkit config
let arToolkitSource = new THREEx.ArToolkitSource({
	sourceType : 'webcam',
})

arToolkitSource.init(function onReady(){
	onResize()
})

// handle resize
window.addEventListener('resize', function(){
	onResize()
})

function onResize(){
	arToolkitSource.onResizeElement();
	arToolkitSource.copyElementSizeTo(renderer.domElement);
	if( arToolkitContext.arController !== null ){
		arToolkitSource.copyElementSizeTo(arToolkitContext.arController.canvas);		
	}
}

// init artoolkit

// create artToolkitContext
let arToolkitContext = new THREEx.ArToolkitContext({
	cameraParametersUrl: '/data/camera_para.dat',
	detectionMode: 'mono',
})
arToolkitContext.init(function onCompleted(){
	// copy projection matrix to camera
	camera.projectionMatrix.copy( arToolkitContext.getProjectionMatrix() );
	camera.updateProjectionMatrix();
	cameraSlice.updateProjectionMatrix();
})


// update artoolkit function
function updateArToolKit() {
	if( arToolkitSource.ready === false)	return
	arToolkitContext.update( arToolkitSource.domElement )
	// update scene.visible if the marker is seen
	scene.visible = camera.visible;
}


// Create a ArMarkerControls

// init controls for camera
let markerControls = new THREEx.ArMarkerControls(arToolkitContext, camera, {
	type : 'pattern',
	patternUrl : '/data/patt.hiro',
	changeMatrixMode: 'cameraTransformMatrix'
})
scene.visible = false

//  add an object in the scene

// Load crate texture
let texture = new THREE.TextureLoader().load('./data/crate.gif');

// Controled object array
let controlledObjects = []

// Add the crate
let cubeGeometry = new THREE.BoxBufferGeometry( 1, 1, 1);
let cubeMaterial = new THREE.MeshBasicMaterial( { map: texture } );
let cube = new THREE.Mesh( cubeGeometry, cubeMaterial );
cube.position.set(0,2,-2);
cube.frustumCulled = false; // useless ?
scene.add(cube);
controlledObjects.push(cube);

let sphere = new Shape("shape1", "sphere");
sphere.setPosition(0, 3.5, -2);
sphere.setDimensions(1,1,1);
scene.add(sphere.getNode());
controlledObjects.push(sphere.getNode());

let texte = new Text("texte","REVEAM");
texte.setPosition(1,2,-2);
texte.setDimensions(0.01,0.01,0.01);
scene.add(texte.getNode());
controlledObjects.push(texte.getNode());

let layers = new Layers("lay","./data/content/brain/brain",99);
layers.setPosition(1,2,-5);
layers.setDimensions(1,1,1);
scene.add(layers.getNode());
controlledObjects.push(layers.getNode());

let model = new Model("model", "./data/content/bunny.obj");
model.setPosition(-0.5, 1, -3);
model.setDimensions(0.01, 0.01, 0.01);
scene.add(model.getNode());
controlledObjects.push(model.getNode());
/*
let videoObj = new Video("video", "./data/content/woodpecker.ogv");
videoObj.setPosition(0, 3, -4);
videoObj.setDimensions(0.02,0.02,0.02);
scene.add(videoObj.getNode());
*/

//	pointdragcontrols setup
let controlsSlice = new DragControls( controlledObjects, cameraSlice, renderer.domElement);
controlsSlice.dispose();
let controls = new DragControls( controlledObjects, camera, renderer.domElement);

// Use which camera ?

// doubletap to swap camera

renderer.domElement.addEventListener("touchstart", tapHandler);

let tapedTwice = false;

function tapHandler(event) {
    if(!tapedTwice) {
        tapedTwice = true;
		setTimeout( function() { tapedTwice = false; }, 300 );

        return false;
    }
	event.preventDefault();
	swapView();
}

function objectSym(){
	scene.children.forEach(obj=> {
		if ((obj != camera) && (obj != cameraSlice)){
			obj.position.multiplyScalar(-1);	
		} 

	});
}

function swapView() {
	if (renderCamera == camera){
		controls.dispose();
		controlsSlice.activate();
		renderCamera = cameraSlice;
		renderer.setClearColor(grey, 1);
	} else {
		controlsSlice.dispose();
		controls.activate();
		renderCamera = camera;
		renderer.setClearColor(white, 0);
	}
	video.hidden = !video.hidden;
	objectSym();
}





// run the rendering loop
function animate() {

	updateArToolKit();

	if (renderCamera == cameraSlice) {
	cameraSlice.position.copy(camera.position);
	cameraSlice.rotation.copy(camera.rotation);
	}
	renderer.render( scene,renderCamera);
	requestAnimationFrame( animate );
}

onResize();
animate();